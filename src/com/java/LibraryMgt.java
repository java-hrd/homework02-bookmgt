package com.java;

import com.java.constants.Constants;
import com.java.service.BookImp;
import com.java.models.Book;
import com.java.utils.UtilKit;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

public class LibraryMgt {
    private Book[] books = new Book[Constants.SIZE_BOOKS];
    private BookImp bookImp = null;
    private static int bookId;
    private String libraryName;
    private String libraryAddress;

    // TODO : set up library
    public void setupLibrary() {
        System.out.println(Constants.SETUP_LIBRARY);
        do {
            libraryName = UtilKit.inputString(Constants.ENTER_LIBRARY_NAME);
        }while(!UtilKit.containsSpece(libraryName));

        do {
            libraryAddress = UtilKit.inputString(Constants.ENTER_LIBRARY_ADDRESS);
        }while(!UtilKit.containsSpece(libraryAddress));

        System.out.println(Constants.SETUP_LIBRARY_SUCCESSFUL(libraryName, libraryAddress, Constants.LIBRARY_CREATED));
    }

    // TODO : display menu in library
    public void displayBook() {
        System.out.println(Constants.LIBRARY_MANAGEMENT_SYSTEM(libraryName, libraryAddress));
        System.out.println(Constants.ADD_BOOK);
        System.out.println(Constants.SHOW_ALL_BOOKS);
        System.out.println(Constants.SHOW_AVAILABLE_BOOKS);
        System.out.println(Constants.BORROW_BOOK);
        System.out.println(Constants.RETURN_BOOK);
        System.out.println(Constants.EXIT);
        System.out.println(Constants.LONG_LINE);
        chooseMenu();
    }

    private void chooseMenu() {
        int chooseNumber = UtilKit.inputNumber(Constants.CHOOSE_OPTION);
        switch (chooseNumber) {
            case 1:
                addBook();
                displayBook();
                break;
            case 2:
                showAllBooks();
                displayBook();
                break;
            case 3:
                showAvailableBook();
                displayBook();
                break;
            case 4:
                borrowBook();
                displayBook();
                break;
            case 5:
                returnBook();
                displayBook();
                break;
            case 6:
                System.out.println(Constants.GOODBYE);
                System.exit(0);
                break;
            default:
                System.out.println(Constants.ALERT_INPUT_NUMBER_1_TO_6);
                chooseMenu();
                break;

        }
    }

    public void addBook() {
        BookImp.libraryImp(bookImp).addBook(books);
    }

    // TODO : show all books
    public void showAllBooks() {
        System.out.println("\n\n"+Constants.DISPLAY_ALL_BOOKS_INFO);
        if (books != null && books[0] != null) {
            displayList(books);
        } else {
            System.out.println(Constants.NO_BOOK);
        }
    }

    static int countAvailable = 0;

    // TODO : show only available books
    public void showAvailableBook() {
        System.out.println("\n\n"+Constants.DISPLAY_AVAILABLE_BOOKS_INFO);
        int count = 0;
        boolean isAvailable = false;

        if (books != null && books[0] != null) {
            Book[] tmp = new Book[books.length];
            for (int i=0; i<UtilKit.countBook; i++) {
                if (books[i].isStatus()) {
                    tmp[count] = books[i];
                    count ++;
                    LibraryMgt.countAvailable ++;
                    isAvailable = true;
                }
            }

            Book[] availableBooks = new Book[count];
            int c = 0;
            for (int i=0; i<tmp.length; i++) {
                if (tmp[i] != null) {
                    availableBooks[c] = tmp[i];
                    c ++;
                }
            }
            if (isAvailable) {
                displayList(availableBooks);
            } else {
                System.out.println(Constants.NO_BOOK);
            }
        } else {
            System.out.println(Constants.NO_BOOK);
        }
    }


    // TODO : borrow book
    public void borrowBook() {
        System.out.println(Constants.BORROW_BOOK_INFO);
        BookImp.libraryImp(bookImp).borrowBook(books);
    }

    // TODO : return book
    public void returnBook() {
        System.out.println(Constants.RETURN_BOOK_INFO);
        BookImp.libraryImp(bookImp).returnBook(books);
    }

    // TODO : display list of books
    public void displayList(Book[] books){
        System.out.println(books.length);
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.LEFT, CellStyle.AbbreviationStyle.CROP,
                CellStyle.NullStyle.EMPTY_STRING);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL, false, "");

        int total;
        if (books.length < UtilKit.countBook) {
            total = books.length;
        } else {
            total = UtilKit.countBook;
        }

        String stStatus;
        t.addCell("   ID   ", cs);
        t.addCell("   TITLE   ", cs);
        t.addCell("   AUTHOR   ", cs);
        t.addCell("   PUBLISHED DATE   ", cs);
        t.addCell("   STATUS   ", cs);
        for (int i=0; i<total; i++) {
            if (books[i].isStatus()) {
                stStatus = "Available";
            } else {
                stStatus = "Unavailable";
            }
            t.addCell(books[i].getId() + "", cs);
            t.addCell(books[i].getTitle(), cs);
            t.addCell(books[i].getAuthor(), cs);
            t.addCell(books[i].getPublishedDate() + "", cs);
            t.addCell(stStatus, cs);


        }
        System.out.println(t.render());
        System.out.println();
        UtilKit.promptEnterKey();
    }

    public Book inputBook() {
        bookId ++;
        String bookName;
        String author;

        System.out.println(Constants.ADD_BOOK_INFO);
        System.out.println(Constants.BOOK_ID + bookId);
        do {
            bookName = UtilKit.inputString(Constants.ENTER_BOOK_NAME);
        }while(!UtilKit.containsSpece(bookName));

        do {
            author = UtilKit.inputString(Constants.ENTER_BOOK_AUTHOR);
        }while(!UtilKit.containsSpece(author));

        int publishedDate = UtilKit.inputNumber(Constants.ENTER_PUBLISHED_YEAR);
        System.out.println(Constants.CREATE_BOOK);
        return new Book(bookId, bookName, author, publishedDate, true);
    }
}
