package com.java;

public class Main {

    public static void main(String[] args) {
        LibraryMgt libraryManagement = new LibraryMgt();
        libraryManagement.setupLibrary();
        libraryManagement.displayBook();
    }
}
