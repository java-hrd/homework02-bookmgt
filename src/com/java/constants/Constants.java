package com.java.constants;

import com.java.models.Book;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.Date;

public class Constants {
    public static final int SIZE_BOOKS = 5;

    public static final String SETUP_LIBRARY = "\n========= SET UP LIBRARY =========";
    public static final String ENTER_LIBRARY_NAME       = "=> Enter Library's Name    : ";
    public static final String ENTER_LIBRARY_ADDRESS    = "=> Enter Library's Address : ";
    public static final String LIBRARY_CREATED = new Date().toString();
    public static final String BOOK_NOT_EXIST(int bookId) {
        return "Book ID : " + bookId + " not Exist...";
    }

    public static String SETUP_LIBRARY_SUCCESSFUL(String libraryName, String libraryAddress, String createdDate){
        return "\"" + libraryName.toUpperCase() + "\" Library is already created in \"" + libraryAddress.toUpperCase() + "\" address successfully on " + createdDate + ".\n";
    }

    public static final String LIBRARY_MANAGEMENT_SYSTEM(String name, String address) {
        return "\n========= " + name.toUpperCase() + " ," + address.toUpperCase() + " =========";
    }

    public static final String ADD_BOOK                = "1- Add Book";
    public static final String SHOW_ALL_BOOKS          = "2- Show All Books";
    public static final String SHOW_AVAILABLE_BOOKS    = "3- Show Available Books";
    public static final String BORROW_BOOK             = "4- Borrow Book";
    public static final String RETURN_BOOK             = "5- Return Book";
    public static final String EXIT                    = "6- Exit";
    public static final String LONG_LINE               = "-----------------------------------------";
    public static final String CHOOSE_OPTION           = "=> Choose option(1-6) : ";

    public static final String ADD_BOOK_INFO         = "\n========= ADD BOOK INFO =========";
    public static final String BOOK_ID              = "=> Book ID              : ";
    public static final String ENTER_BOOK_NAME      = "=> Enter Book's Name    : ";
    public static final String ENTER_BOOK_AUTHOR    = "=> Enter Book Author    : ";
    public static final String ENTER_PUBLISHED_YEAR = "=> Enter Published Year : ";
    public static final String FULL_CANNOT_ADD_NEW  = "    Full, Cannot add new book... ";
    public static final String CREATE_BOOK  = "    Book is added successfully ";

    public static final String DISPLAY_ALL_BOOKS_INFO         = "\n========= ALL BOOKS INFO =========";
    public static final String DISPLAY_AVAILABLE_BOOKS_INFO   = "\n========= AVAILABLE BOOKS INFO =========";
    public static final String NO_BOOK                 = "       No Book Available";

    public static final String BORROW_BOOK_INFO         = "\n========= BORROW BOOK INFO =========";
    public static final String ENTER_BOOK_ID_TO_BORROW = "=> Enter Book ID to Borrow : ";
    public static String MSG_NO_AVAILABLE_TO_BORROW(int bookId){
        return "Book ID : " + bookId + " is unavailable...";
    }
    public static String MSG_BORROWED_SUCCESSFULLY(Book book){
        return  "Book ID        : " + book.getId() + "\n" +
                "Book Title     : " + book.getTitle() + "\n" +
                "Book Author    : " + book.getAuthor() + "\n" +
                "Published Year : " + book.getPublishedDate() + " is borrowed successfully...";
    }

    public static final String RETURN_BOOK_INFO         = "\n========= RETURN BOOK INFO =========";
    public static final String ENTER_BOOK_ID_TO_RETURN = "=> Enter Book ID to Return : ";
    public static String MSG_RETURN_SUCCESSFULLY(Book book){
        return  "Book ID        : " + book.getId() + "\n" +
                "Book Title     : " + book.getTitle() + "\n" +
                "Book Author    : " + book.getAuthor() + "\n" +
                "Published Year : " + book.getPublishedDate() + " is returned successfully...";
    }
    public static String MSG_RETURN_UN_SUCCESSFULLY(int bookId){
        return "Book ID : " + bookId + " is failed to return...";
    }

    public static final String GOODBYE                 = "(^-^) Good Bye! (^-^)";
    public static final String ALERT_INPUT_NUMBER_1_TO_6      = "Please input number 1 to 6";

    public static final void DISPLAY_NONE(String msg) {
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.LEFT, CellStyle.AbbreviationStyle.CROP,
                CellStyle.NullStyle.EMPTY_STRING);
        CellStyle cs1 = new CellStyle(CellStyle.HorizontalAlign.CENTER, CellStyle.AbbreviationStyle.CROP,
                CellStyle.NullStyle.EMPTY_STRING);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL, false, "");

        t.addCell("   ID   ", cs);
        t.addCell("   TITLE   ", cs);
        t.addCell("   AUTHOR   ", cs);
        t.addCell("   PUBLISHED DATE   ", cs);
        t.addCell("   STATUS   ", cs);

        t.addCell(msg, cs1);
        System.out.println(t.render());
        System.out.println();
    }
}
