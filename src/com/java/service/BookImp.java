package com.java.service;

import com.java.constants.Constants;
import com.java.models.Book;
import com.java.LibraryMgt;
import com.java.utils.UtilKit;

public class BookImp {
    private LibraryMgt libraryMgt;

    public BookImp() {
        this.libraryMgt = new LibraryMgt();
    }

    public static BookImp libraryImp(BookImp bookImp) {
        if (bookImp == null) {
            return new BookImp();
        } else {
            return bookImp;
        }
    }


    // TODO : add book
    public void addBook (Book[] books) {
        if (books.length > UtilKit.countBook) {
            Book book = libraryMgt.inputBook();
            books[UtilKit.countBook] = book;
            UtilKit.countBook ++;
        } else {
            System.out.println(Constants.FULL_CANNOT_ADD_NEW);
        }
    }


    // TODO : borrow book
    public void borrowBook(Book[] books) {
        int status = 0;
        Book book = new Book();
        if (books != null && books[0] != null) {
            int tmpBookId = UtilKit.inputNumber(Constants.ENTER_BOOK_ID_TO_BORROW);
            for (int i=0; i< UtilKit.countBook; i++) {
                if (books[i].getId() == tmpBookId) {
                    if (books[i].isStatus()) {
                        books[i].setStatus(false);
                        book.setId(tmpBookId);
                        book.setTitle(books[i].getTitle());
                        book.setAuthor(books[i].getAuthor());
                        book.setPublishedDate(books[i].getPublishedDate());
                        status = 1;
                        break;
                    } else {
                        status = 2;
                        return;
                    }
                } 
            }
            if (status == 1) {
                System.out.println(Constants.MSG_BORROWED_SUCCESSFULLY(book));
            } else if (status == 2) {
                System.out.println(Constants.MSG_NO_AVAILABLE_TO_BORROW(tmpBookId));
            } else {
                System.out.println(Constants.BOOK_NOT_EXIST(tmpBookId));
            }
        } else {
            System.out.println(Constants.NO_BOOK);
        }
    }

    // TODO : return book
    public void returnBook(Book[] books) {
        Book book = new Book();
        int status = 0;
        if (books != null && books[0] != null) {
            int tmpBookId = UtilKit.inputNumber(Constants.ENTER_BOOK_ID_TO_RETURN);
            for (int i=0; i< UtilKit.countBook; i++) {
                if (books[i].getId() == tmpBookId) {
                    if (!books[i].isStatus()) {
                        books[i].setStatus(true);
                        book.setId(tmpBookId);
                        book.setTitle(books[i].getTitle());
                        book.setAuthor(books[i].getAuthor());
                        book.setPublishedDate(books[i].getPublishedDate());
                        status = 1;
                        break;
                    } else {
                        status = 2;
                    }
                }
            }
            if (status == 1) {
                System.out.println(Constants.MSG_RETURN_SUCCESSFULLY(book));
            } else if (status == 2) {
                System.out.println(Constants.MSG_RETURN_UN_SUCCESSFULLY(tmpBookId));
            } else {
                System.out.println(Constants.BOOK_NOT_EXIST(tmpBookId));
            }
        } else {
            System.out.println(Constants.NO_BOOK);
        }
    }
}
