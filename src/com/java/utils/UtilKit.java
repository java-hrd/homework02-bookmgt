package com.java.utils;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilKit {
    public static int countBook = 0;

    static boolean isNumber(String string) {
        int d;
        try
        {
            d = Integer.parseInt(string);
            if (d < 0){
                System.out.println("INPUT IS NEGATIVE NUMBER.");
                return false;
            }else if (d == 0){
                System.out.println("INPUT IS ZERO.");
                return false;
            }else if(string.isEmpty()){
                return false;
            }
        }
        catch(NumberFormatException nfe)
        {
            if(string.isEmpty()) {return false;}
            System.out.println("INPUT IS INVALID.");
            return false;
        }
        return true;
    }

    static boolean isNumberFrom0(String string) {
        int d;
        try
        {
            d = Integer.parseInt(string);
            if (d < 0){
                System.out.println("INPUT IS NEGATIVE NUMBER.");
                return false;
            }else if(string.isEmpty()) {
                return false;
            }
        }
        catch(NumberFormatException nfe)
        {
            System.out.println("INPUT IS INVALID.");
            return false;
        }
        return true;
    }



    public static void promptEnterKey(){
        System.out.println("\nPress \"ENTER\" to continue...");
        try{  System.in.read();}catch(Exception e){	e.printStackTrace();}
        System.out.println("\n\n");
    }

    public static boolean containsSpece(String str) {
        if(str == "" | str.isEmpty()){
            return false;
        }
        return true;
    }

    public static int inputNumber(String label) {

        Scanner scanner = new Scanner(System.in);

        String temp;
        do {
            System.out.print(label);
            temp = scanner.nextLine();
        } while (!UtilKit.isNumber(temp));

        return Integer.parseInt(temp);

    }

    public static String inputString(String label){
        Scanner scanner = new Scanner(System.in);
        String temp;
        System.out.print(label);
        temp = scanner.nextLine();
        return temp;
    }
}
